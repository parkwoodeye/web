<html>
<head>
<?php $title = 'Ophthalmology Services and Eye Care';?>
<title><?php echo "$title"; ?> - Parkwood Eye Center - Elkin, NC</title>
<meta name="description" content="Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="770" height="198" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td width="243" height="145"> <a href="/"><img src="images/parkwoodeyecenterlogo.jpg" alt="Back to Parkwood Eye Center Home Page." width="243" height="145" border="0"></a></td>
    <td rowspan="2"> <a href="/"><img src="images/random/doctorwellsstewart.jpg" alt="Parkwood Eye Center - Serving the Yadkin Valley - Back to Home Page." width="527" height="198" hspace="0" border="0"></a></td>
  </tr>
  <tr>
    <td><div align="center"><span class="size18times"><span class="size24">(336) 835-3400</span></span></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="middle" bgcolor="#000066"><div align="center"><?php include("includes/topnav.htm"); ?></div></td>
    <td width="200" rowspan="2" align="center" valign="top" bgcolor="#003300"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td align="center" valign="top"><?php include("includes/rightnav.htm"); ?></td>
      </tr>
    </table>    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td align="left" valign="top"><h1 class="size18bold greentext"><?php echo "$title"; ?> </h1>
          <p>Parkwood Eye Center is dedicated to excellence in eye care for all ages. We offer the latest in laser and surgical technology in a caring and patient-oriented environment. </p>
          <p><strong><span class="size18bold greentext"><img src="images/oldercouplewithglasses.jpg" alt="Opthalmology Services by Parkwood Eye Center in Elkin, NC." width="200" height="264" hspace="7" vspace="7" align="right"></span>Ophthalmology Services </strong></p>
          <p>The services offered by our office include:</p>
          <ul>
            <li>Routine Eye Examinations</li>
            <li>Eye Glasses</li>
            <li>Contact Lenses</li>
            <li>Pediatric Eye Exams/Surgery</li>
            <li>Laser Surgery</li>
            <li>Macular Degeneration</li>
            <li>Treatment for Dry Eye </li>
            <li>Cataract Extraction</li>
            <li>Posterior Capsulotomy</li>
            <li>Intraocular Lens Implants</li>
            <li>Glaucoma Treatment</li>
            <li>Glaucoma Surgery</li>
            <li>Chalazion Treatment/Surgery</li>
            <li>Detached or Torn Retina</li>
            <li>Treatment of Corneal Disease </li>
            <li>Blepharitis Treatment </li>
            <li>Diabetic Retinopathy</li>
          </ul>          
          <p><strong>Hospital Privileges in Elkin, Mt. Airy, &amp; Sparta, NC </strong></p>
          <p>Dr. Stewart, Dr. Cid, and Dr. Johnston have privileges at <a href="http://www.hughchatham.org" target="_blank">Hugh Chatham Memorial Hospital</a> in Elkin, NC. Dr. Stewart also has privileges at <a href="http://www.northernhospital.com/" target="_blank">Northern Hospital of Surry County</a> in Mt Airy, NC and  <a href="http://www.amhsparta.org/" target="_blank">Alleghany Memorial Hospital</a> in Sparta, NC.</p>
          <p>Surgical procedures, such as cataract extraction and intraocular lens implantation, are performed at <a href="http://www.hughchatham.org" target="_blank">Hugh Chatham</a>, <a href="http://www.northernhospital.com/" target="_blank">Northern Hospital of Surry County</a>, and <a href="http://www.amhsparta.org/" target="_blank">Alleghany Memorial</a> hospitals.</p>
          <p align="right">Call today to schedule an appointment  - <strong>(336) 835-3400</strong></p></td>
      </tr>
    </table>      </td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="images/parkwood_image.gif" width="450" height="10"></td>
    <td width="200" align="center" valign="top" bgcolor="#003300"><img src="images/parkwood_image.gif" width="1" height="1"></td>
  </tr>
</table>
<?php include("includes/footer.htm"); ?>
<div align="center"></div>
<p align="center">&nbsp;</p>
<p align="center" class="size10font">
  <?php include("includes/mwd.htm"); ?>
</p>
<p align="center" class="size10font">&nbsp;</p>

</body>
</html>
