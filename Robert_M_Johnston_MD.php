<html>
<head>
<?php $title = 'Robert M. Johnston, MD - Ophthalmologist';?>
<title><?php echo "$title"; ?> - Parkwood Eye Center - Elkin, NC</title>
<meta name="description" content="Robert M. Johnston - Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Robert M. Johnston, Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="770" height="198" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td width="243" height="145"> <a href="/"><img src="images/parkwoodeyecenterlogo.jpg" alt="Back to Parkwood Eye Center Home Page." width="243" height="145" border="0"></a></td>
    <td rowspan="2"> <img src="images/random/blueridgemtns.jpg" alt="Parkwood Eye Center - Serving the Yadkin Valley - Back to Home Page." width="527" height="198" hspace="0" border="0"></td>
  </tr>
  <tr>
    <td><div align="center"><span class="size18times"><span class="size24">(336) 835-3400</span></span></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="middle" bgcolor="#000066"><div align="center"><?php include("includes/topnav.htm"); ?></div></td>
    <td width="200" rowspan="2" align="center" valign="top" bgcolor="#003300"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td align="center" valign="top"><p class="topnavlinks"><span class="rightnav"><br>
          Our doctors look forward to serving your eye care needs.</span></p>
          <p class="topnavlinks"><hr></p>
          <p class="topnavlinks">
            <?php include("includes/rightnav.htm"); ?>
          </p></td>
      </tr>
    </table>    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td align="left" valign="top"><table width="100%"  border="0" align="center" cellpadding="10" cellspacing="0">
            <tr>
              <td width="50%"><div align="center">
                <h2><img src="images/drjohnston.jpg" alt="Robert M. Johnston, MD - Medical Eye Doctor - Opthamologist." width="175" height="225" hspace="7" vspace="7" align="right">
                  <strong>Robert M. Johnston, MD </strong></h2>
                </div>
                  <p>Dr. Johnston grew up in Connecticut and received his Bachelor of Science degree in physics from

                  Massachusetts Institute of Technology.  He earned his M.D. degree from Johns Hopkins University

                  School of Medicine in Baltimore and completed his internship at Surgical Queens Hospital in Hawaii.  He

                  completed his ophthalmology residency at Yale University New Haven Hospital and then served as Lt.

                  Commander and Ophthalmologist at the United States Coast Guard Academy Hospital in New London,

                  CT.</p>

                  <p>Dr. Johnston practiced ophthalmology in northern Virginia for over 30 years where he specialized in

                  surgery and diseases of the eye, with a concentration in cataract and refractive surgery.  Dr. Johnston is

                  certified by the American Board of Ophthalmology.  He is a Fellow of the American Academy of

                  Ophthalmology and the American College of Eye Surgeons.  He enjoys sailing when his schedule permits.<p>
                <p>Call today to schedule an appointment with Dr. Johnston - <strong>(336) 835-3400</strong></p></td>
              </tr>
          </table>          
          </td>
      </tr>
    </table>      </td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="images/parkwood_image.gif" width="450" height="10"></td>
    <td width="200" align="center" valign="top" bgcolor="#003300"><img src="images/parkwood_image.gif" width="1" height="1"></td>
  </tr>
</table>
<?php include("includes/footer.htm"); ?>
<div align="center"></div>
<p align="center">&nbsp;</p>
<p align="center" class="size10font">
  <?php include("includes/mwd.htm"); ?>
</p>
<p align="center" class="size10font">&nbsp;</p>

</body>
</html>
