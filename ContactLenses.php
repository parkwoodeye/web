<html>
<head>
<?php $title = 'Contact Lenses - New and Replacement';?>
<title><?php echo "$title"; ?> - Parkwood Eye Center - Elkin, NC</title>
<meta name="description" content="Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="770" height="198" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td width="243" height="145"> <a href="/"><img src="images/parkwoodeyecenterlogo.jpg" alt="Back to Parkwood Eye Center Home Page." width="243" height="145" border="0"></a></td>
    <td rowspan="2"> <a href="/"><img src="images/random/olderladyeyes.jpg" alt="Parkwood Eye Center - Serving the Yadkin Valley - Back to Home Page." width="527" height="198" hspace="0" border="0"></a></td>
  </tr>
  <tr>
    <td><div align="center"><span class="size18times"><span class="size24">(336) 835-3400</span></span></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="middle" bgcolor="#000066"><div align="center"><?php include("includes/topnav.htm"); ?></div></td>
    <td width="200" rowspan="2" align="center" valign="top" bgcolor="#003300"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td align="center" valign="top"><?php include("includes/rightnav.htm"); ?></td>
      </tr>
    </table>    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td align="left" valign="top"><h1 class="size18bold greentext"><?php echo "$title"; ?> </h1>
          <p>Parkwood Eye Center of Elkin, NC, offers comprehensive contact lens care which includes:</p>
          <ul>
            <li>
              <h4>Contact Lens Instruction</h4>
            </li>
            <li>
              <h4>Contact Lens Consultation </h4>
            </li>
            <li>
              <h4>Education About Contact Lenses</h4>
            </li>
            <li>
              <h4>Custom Fitting of Lenses </h4>
            </li>
            <li>
              <h4>Follow-Up Appointments</h4>
            </li>
            <li>
              <h4>Lens Ordering &amp; Reordering</h4>
            </li>
            </ul>          
          <p>If you have a question, would like to discuss switching to contact lenses from eye glasses or are a current contact lens patient at Parkwood Eye Center, please feel free to give us a call (<strong>336-835-3400</strong>) or <a href="DrivingDirections.php">come by our office</a> to discuss your lens needs with on of our contact lens specialists.</p></td>
      </tr>
    </table>      </td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="images/parkwood_image.gif" width="450" height="10"></td>
    <td width="200" align="center" valign="top" bgcolor="#003300"><img src="images/parkwood_image.gif" width="1" height="1"></td>
  </tr>
</table>
<?php include("includes/footer.htm"); ?>
<div align="center"></div>
<p align="center">&nbsp;</p>
<p align="center" class="size10font">
  <?php include("includes/mwd.htm"); ?>
</p>
<p align="center" class="size10font">&nbsp;</p>

</body>
</html>
