<html>
<head>
<?php $title = 'Driving Directions to Our Office';?>
<title><?php echo "$title"; ?> - Parkwood Eye Center - Elkin, NC</title>
<meta name="description" content="Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="770" height="198" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td width="243" height="145"> <a href="/"><img src="images/parkwoodeyecenterlogo.jpg" alt="Back to Parkwood Eye Center Home Page." width="243" height="145" border="0"></a></td>
    <td rowspan="2"> <a href="/"><img src="images/random/parkwood.jpg" alt="Parkwood Eye Center - Serving the Yadkin Valley - Back to Home Page." width="527" height="198" hspace="0" border="0"></a></td>
  </tr>
  <tr>
    <td><div align="center"><span class="size18times"><span class="size24">(336) 835-3400</span></span></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="middle" bgcolor="#000066"><div align="center"><?php include("includes/topnav.htm"); ?></div></td>
    <td width="200" rowspan="2" align="center" valign="top" bgcolor="#003300"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td align="center" valign="top"><?php include("includes/rightnav.htm"); ?></td>
      </tr>
    </table>    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td align="left" valign="top"><h1 class="size18bold greentext"><?php echo "$title"; ?> </h1>
          <p>For turn by turn driving directions to our location, simply use one of the  providers below - our address* has already been provided - simply enter your starting address for detailed driving directions.</p>
          <p align="center"><a href="http://maps.google.com/maps?q=177+Parkwood+Dr,+Elkin,+NC+28621,+USA&sa=X&oi=map&ct=title" target="_blank"></a> <a href="http://maps.yahoo.com/#q1=119+Parkwood+Drive,+elkin,+nc&mvt=m&trf=0&lon=-80.837145&lat=36.264795&mag=3" target="_blank"><strong>Yahoo Maps</strong></a><strong> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <a href="http://www.mapquest.com/directions/main.adp?src=maps&1sb=revise&2l=PcZfzbed7IEFbvoSjTsPAA==&2g=f+DIXlWtNuH5GVE9qH/v3g==&2v=ADDRESS&2a=119%20Parkwood%20Dr&2c=Elkin&2s=NC&2z=28621-2429&2y=US&2pn=&2pl=&cat=&q=" target="_blank">MapQuest</a> &nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;<a href="http://maps.google.com/maps?q=119+Parkwood+Dr,+Elkin,+NC+28621,+USA&sa=X&oi=map&ct=title" target="_blank">Google Maps</a></strong></p>
          <p align="center">&nbsp;</p>          
          <p align="justify">*Due to inaccurate map placements on all three mapping sites for our true address, we have provided our address as <em>119 Parkwood Drive in the above links to get you to the right entrance of Parkwood Drive</em>, however we are actually located at:</p>
          <p align="center"><strong>177 Parkwood Drive<br>
            Elkin, NC 28621</strong></p>
          <p align="center"><a href="http://maps.yahoo.com/#q1=119+Parkwood+Drive,+elkin,+nc&mvt=m&trf=0&lon=-80.837145&lat=36.264795&mag=3" target="_blank"><img src="images/maptoparkwoodeyecenter.gif" alt="Come See Us at Parkwood Eye Center For All Your Eyecare Needs." width="469" height="362" border="0"></a></p></td>
      </tr>
    </table>      </td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="images/parkwood_image.gif" width="450" height="10"></td>
    <td width="200" align="center" valign="top" bgcolor="#003300"><img src="images/parkwood_image.gif" width="1" height="1"></td>
  </tr>
</table>
<?php include("includes/footer.htm"); ?>
<div align="center"></div>
<p align="center">&nbsp;</p>
<p align="center" class="size10font">
  <?php include("includes/mwd.htm"); ?>
</p>
<p align="center" class="size10font">&nbsp;</p>

</body>
</html>
