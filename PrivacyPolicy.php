<html>
<head>
<?php $title = 'Notice of Privacy Policies and Practices';?>
<title><?php echo "$title"; ?> - Parkwood Eye Center - Elkin, NC</title>
<meta name="description" content="Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="770" height="198" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td width="243" height="145"> <a href="/"><img src="images/parkwoodeyecenterlogo.jpg" alt="Back to Parkwood Eye Center Home Page." width="243" height="145" border="0"></a></td>
    <td rowspan="2"> <a href="/"><img src="images/random/biking.jpg" alt="Parkwood Eye Center - Serving the Yadkin Valley - Back to Home Page." width="527" height="198" hspace="0" border="0"></a></td>
  </tr>
  <tr>
    <td><div align="center"><span class="size18times"><span class="size24">(336) 835-3400</span></span></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="middle" bgcolor="#000066"><div align="center"><?php include("includes/topnav.htm"); ?></div></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td align="left" valign="top"><h1 align="center" class="size18bold greentext"><?php echo "$title"; ?></h1>
          <p align="left"><strong>Dear Patient,</strong></p>
          <p align="left">This notice describes how information about you may be used and disclosed, and  how you can get access to this information. </p>          <p align="center"><strong><u>Please Review This Information Carefully</u></strong></p>
          <p>Our physicians and staff are committed to  using your personal health information responsibly. This notice describes how and when we use or disclose this information. This notice is effective April 14, 2003, and applies to all of your protected health information (PHI) as defined by federal regulations. </p>
          <p align="center"><strong><u>Your Medical Record</u> </strong></p>
          <p>Each time you visit our office, a record of your visit is made. Typically, this record contains information about your visit, including your exam, diagnosis, test results, treatment and other pertinent health data. This  information, often referred to as your medical record and  personal health  information (PHI) serves as a: </p>
          <ul>
            <li>Basis for planning your care and treatment </li>
            <li>Means of communication with other health professionals involved in your  care </li>
            <li>Legal document describing the care you  received </li>
            <li>Document that you or health insurance plans use to verify that services billed were actually provided </li>
            <li>Tool that we can use to ensure high quality care and patient satisfaction. </li>
          </ul>          <p align="left">Understanding what is in your record and how your health information is used helps you to ensure its accuracy, determine who has access to it, and make informed decisions when authorizing us to disclose this information to others. </p>
          <p align="center"><strong><u>YOUR RIGHTS</u></strong></p>
          <p align="left">You have certain rights under the federal privacy standards. These include the rights to:</p>
          <ul>
            <li>Request restrictions on the use of your protected health information </li>
            <li>Receive confidential communication concerning your medical condition and treatment. </li>
            <li>Inspect and receive a copy of your protected health information with certain exceptions. </li>
            <li>Request amendment or correction to your protected health information </li>
            <li>Receive an accounting of how and to whom your protected health information has been disclosed </li>
            <li>Receive a printed copy of this Notice </li>
            </ul>          
          <p align="center"><strong><u>OUR RESPONSIBILITIES</u></strong></p>
          <p>Parkwood Eye Center is required to: </p>
          <ul>
            <li>Maintain the privacy of your health information </li>
            <li>Provide you with this Notice of our legal duties and privacy practices with respect to information that we collect and  maintain about you </li>
            <li>Abide by the terms and obligations of this Notice </li>
            <li>Notify you if we are unable to agree to a request </li>
            <li>Accommodate reasonable requests you may have regarding communication of health  information by alternative means. </li>
            </ul>          
          <p>As permitted by law, we reserve the right to amend or modify our privacy policies and   practices. These changes in our policies and practices may be required by changes in federal and state laws and regulations. When revisions  occur, we will provide you with a revised notice on your next visit to our office. The revised policies and practices will  apply to all    protected health information that we maintain. We will not use or disclose your health  information without your authorization, except as described in this Notice. We will also stop using or disclosing your health information after we have received a written revocation of the authorization according to procedures included in the authorization. </p>
          <p align="center"><strong><u>HOW WE MAY USE OR DISCLOSE YOUR HEALTH INFORMATION</u> </strong></p>
          <p><strong>Treatment.  </strong>Your health information may be used by your physician, our staff and other health care professionals outside our office involved in your health care for the purpose of providing health care services to you. </p>
          <p><strong>Payment. </strong> Your health insurance plan including Worker s Compensation may request and receive information on dates of service, services provided and the medical condition being treated in order to pay  for the services you receive. </p>
          <p><strong>Healthcare Operations.  </strong>Your health information may be used to make business decisions necessary for the management, operation and development of the practice. For example, your health information may be used to verify and 


 improve quality of care, train and evaluate employees and  manage business operations. </p>
          <p><strong>Business Associates.</strong>  We will share your protected health information with third party  business associates that perform various activities for the practice such as billing and transcription services. Whenever an arrangement between our office and a business associate involves the use or disclosure of your protected health information, we will have a written contract that contains terms that will protect the privacy of your protected health information. </p>
          <p><strong>Appointment Reminders. </strong> The practice may use your information to remind you about your  upcoming appointments. A brief message may be left on your answering machine. If you don't approve of this method, please advise us. </p>
          <p align="center"><strong><u>Others Involved in Your Healthcare:</u></strong></p>
          <ul>
            <li><strong>Family. </strong>We may use our best judgment in disclosing information to a family member or any other person involved in your care or any person you  authorize to receive  your information. Please inform us if you do not want a family member or other person to have access to your health information. </li>
            <li><strong>Research.</strong> We may disclose your protected health information to researchers when their research has been approved by an Institutional Review Board that has reviewed the research proposal and established protocols to ensure the privacy of your protected health information. </li>
            <li><strong>Public Health.</strong> We may disclose your health information to public health or  legal authorities as required by law. </li>
            <li> <strong>Required by Law. </strong>We may disclose your health information to the extent required by law. </li>
            <li><strong>Healthcare Oversight. </strong>We may disclose your information to a health oversight agency for activities authorized by law. </li>
            <li><strong>Emergencies. </strong>We may use or disclose your protected health information in an emergency treatment situation. </li>
          </ul>                    <p align="center"><strong><u>Other Uses And Disclosures</u></strong></p>
          <ul>
            <li> <strong>Abuse or Neglect. </strong>We may disclose your protected health information to a public health authority authorized by law to receive reports of abuse, neglect or domestic violence consistent with requirements of federal and state laws. </li>
            <li><strong>Food and Drug Administration.</strong> We may disclose your information to a person or company required by FDA to report adverse events, product defects or recalls or other required reports. </li>
            <li><strong>Coroners, Funeral Directors and Organ Donation. </strong>We may disclose your health information for identification purposes, for organ donation purposes or as otherwise authorized by law. </li>
            <li><strong>Legal Proceedings.</strong> We may disclose your health information in the course of any judicial or administrative proceeding in response to a court order, subpoena or other lawful process. </li>
            <li><strong>Criminal Activity. </strong>Consistent with federal and state laws, we may disclose your information if we believe that it is necessary to prevent or lessen a serious and imminent threat to the health or safety of a person or the public. We may disclose your information if it is necessary for law enforcement authorities to identify or apprehend an individual. </li>
            <li><strong>Inmates.</strong> Should you be an inmate of a correctional institution, we may disclose information necessary for your health and the health and safety of others. </li>
            <li><strong>Military Activity and National Security. </strong>We may use or disclose protected health information of individuals who are Armed Forces personnel for activities deemed necessary by military authorities and for conducting national security and intelligence activities.</li>
            <li><strong>Required Uses and Disclosures. </strong>Under the law we must make disclosures to you, and when required  to the Secretary  Department of Health and Human Services, to investigate or determine our compliance with Federal law. </li>
            <li><strong>Other. </strong>Other uses and disclosures of your protected health information will be made only with your written authorization, unless otherwise permitted or required by law. You may revoke this authorization at any time, in writing, except to the extent that your physician or the practice has already taken an action on the use of your previously signed authorization. </li>
          </ul>          <p align="center"><strong> <u>FOR MORE INFORMATION OR TO REPORT A PROBLEM:</u></strong></p>
          <p>If you have complaints, problems or would like more information, please contact our Privacy Officer at <strong>336-835-3400</strong> or at <a href="DrivingDirections.php">177 Parkwood Drive, Elkin, NC</a>. If you believe your privacy rights have been violated, please contact our Privacy Officer or you may file a complaint with the US Department of Health and Human Services. </p></td>
      </tr>
    </table>      </td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="images/parkwood_image.gif" width="450" height="10"></td>
    <td width="200" align="center" valign="top" bgcolor="#003300"><img src="images/parkwood_image.gif" width="1" height="1"></td>
  </tr>
</table>
<?php include("includes/footer.htm"); ?>
<div align="center"></div>
<p align="center"></p>
<p align="center" class="size10font">
  <?php include("includes/mwd.htm"); ?>
</p>
<p align="center" class="size10font"></p>

</body>
</html>
