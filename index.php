<html>
<head>
<?php $title = 'Eye Care in the Yadkin Valley of North Carolina';?>
<title>Parkwood Eye Center - Elkin, NC - <?php echo "$title"; ?></title>
<meta name="description" content="Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta name="google-site-verification" content="xeNTkSUC0INTZao_n-UF1okMwqhlQp6aDYBLCIkRCyM" />
<meta name="google-site-verification" content="C_rgltCtbIXK7UEjvR5drePUqgs1FOTuYwoi-aEzr8E" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td> <img src="images/parkwood_01.jpg" width="241" height="122" alt="Parkwood Eye Center - Elkin, NC, Eye Doctors."></td>
    <td width="529" align="center" valign="bottom">
      <div align="center">
        <p align="right" class="size18times"> Dedicated to Excellence in Eye Care&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <span class="size24">(336) 835-3400&nbsp;&nbsp;&nbsp;</span></p>
        <table width="100%"  border="0" cellpadding="2" cellspacing="0" bgcolor="#000066">
          <tr>
            <td height="45" colspan="5" align="center" valign="middle"><div align="center">
              <?php include("includes/topnavhome.htm"); ?>
            </div>              </td>
          </tr>
        </table>
      <img src="images/parkwood_image.gif" width="400" height="13"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
		<td width="770" height="400" align="left" valign="bottom" background="images/parkwood_03.jpg"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="770" height="400" id="parkwoodintro" align="middle">
            <img style="width: 100%; height: 100%;" alt="" title="" src="images/mainslide.png" />
		</td>
          </tr>
        </table>
	  </td>
	</tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>

<div align="center">
  <?php include("includes/footer.htm"); ?>
  
  <div align="center"></div>
  <p align="center">&nbsp;</p>
  <p align="center" class="size10font">
    <?php include("includes/mwd.htm"); ?>
  </p>
  <p align="center" class="size10font">&nbsp;</p>
  <p align="center" class="size10font">
    <?php include("includes/footerplus.htm"); ?>
</p>
  
</div>
</body>
</html>