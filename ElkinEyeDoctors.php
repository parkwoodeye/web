<html>
<head>
<?php $title = 'Meet Your Local Elkin Eye Doctors';?>
<title><?php echo "$title"; ?> - Parkwood Eye Center - Elkin, NC</title>
<meta name="description" content="Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="770" height="198" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td width="243" height="145"> <a href="/"><img src="images/parkwoodeyecenterlogo.jpg" alt="Back to Parkwood Eye Center Home Page." width="243" height="145" border="0"></a></td>
    <td rowspan="2"> <img src="images/random/blueridgemtns.jpg" alt="Parkwood Eye Center - Serving the Yadkin Valley - Back to Home Page." width="527" height="198" hspace="0" border="0"></td>
  </tr>
  <tr>
    <td><div align="center"><span class="size18times"><span class="size24">(336) 835-3400</span></span></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="middle" bgcolor="#000066"><div align="center"><?php include("includes/topnav.htm"); ?></div></td>
    <td width="200" rowspan="2" align="center" valign="top" bgcolor="#003300"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td align="center" valign="top"><p class="topnavlinks"><span class="rightnav"><br>
Our doctors look forward to serving your eye care needs.</span>          </p>
<p class="topnavlinks"><a href="http://nclnet.org/health/eyes/providers.htm" target="_blank"></a></p>
          <p class="topnavlinks"><hr></p>
          <p class="topnavlinks">
            <?php include("includes/rightnav.htm"); ?>
          </p></td>
      </tr>
    </table>    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td align="left" valign="top"><h1 align="center" class="size18bold greentext"><?php echo "$title"; ?> </h1>
          <table width="100%"  border="0" align="center" cellpadding="10" cellspacing="0">
              <tr align="center" valign="top">
                  <td width="33%"><div align="center"> <a href="Wells_Stewart_MD_Ophthalmologist.php"><img src="images/WellsStewartMD.jpg" alt="Wells Stewart, MD - Medical Eye Doctor - Opthamologist." width="135" vspace="3" border="0"></a><br>
                          <strong><a href="Wells_Stewart_MD_Ophthalmologist.php" class="greentext">Wells Stewart, MD</a></strong></div></td>
                  <td width="33%">
                    <div align="center"> <a href="Monica_Cid_MD_Ophthalmologist.php"><img src="images/photo_Monica_Cid_MD_Ophthalmologist.jpg" alt="Monica Cid, MD - Medical Eye Doctor - Opthamologist." width="135" vspace="3" border="0"><br>
                      <strong>Monica Cid, MD</strong></a><strong> </strong></div>
                  </td>
                  <td width="33%" align="center">
                      <div align="center"> <a href="Robert_M_Johnston_MD.php"><img src="images/drjohnston.jpg" alt="Robert M Johnston" width="135" vspace="3" border="0"></a><br>
                          <strong><a href="Robert_M_Johnston_MD.php" class="greentext">Robert M. Johnston, MD</a></strong></div>
                  </td>
              </tr>
          </table>
          </td>
      </tr>
    </table>      </td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="images/parkwood_image.gif" width="450" height="10"></td>
    <td width="200" align="center" valign="top" bgcolor="#003300"><img src="images/parkwood_image.gif" width="1" height="1"></td>
  </tr>
</table>
<?php include("includes/footer.htm"); ?>
<div align="center"></div>
<p align="center">&nbsp;</p>
<p align="center" class="size10font">
  <?php include("includes/mwd.htm"); ?>
</p>
<p align="center" class="size10font">&nbsp;</p>

</body>
</html>
