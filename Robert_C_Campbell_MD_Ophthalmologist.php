<html>
<head>
<?php $title = 'Robert C. Campbell - Ophthalmologist';?>
<title><?php echo "$title"; ?> - Parkwood Eye Center - Elkin, NC</title>
<meta name="description" content="Robert C. Campbell - Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Robert C. Campbell, Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="770" height="198" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td width="243" height="145"> <a href="/"><img src="images/parkwoodeyecenterlogo.jpg" alt="Back to Parkwood Eye Center Home Page." width="243" height="145" border="0"></a></td>
    <td rowspan="2"> <img src="images/random/blueridgemtns.jpg" alt="Parkwood Eye Center - Serving the Yadkin Valley - Back to Home Page." width="527" height="198" hspace="0" border="0"></td>
  </tr>
  <tr>
    <td><div align="center"><span class="size18times"><span class="size24">(336) 835-3400</span></span></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="middle" bgcolor="#000066"><div align="center"><?php include("includes/topnav.htm"); ?></div></td>
    <td width="200" rowspan="2" align="center" valign="top" bgcolor="#003300"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td align="center" valign="top"><p class="topnavlinks"><span class="rightnav"><br>
          Our doctors look forward to serving your eye care needs.</span></p>
          <p class="topnavlinks"><hr></p>
          <p class="topnavlinks">
            <?php include("includes/rightnav.htm"); ?>
          </p></td>
      </tr>
    </table>    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td align="left" valign="top">            <table width="100%"  border="0" align="center" cellpadding="10" cellspacing="0">
              <tr>
                <td width="50%"><div align="center">
                    <h2 align="center"><strong><img src="images/BobCampbellMD.jpg" alt="Bob Cambell, MD - Medical Eye Doctor - Opthamologist." width="175" height="225" vspace="3" align="right">Robert C. Campbell, MD</strong> </h2>
                  </div>
                    <p>Dr. Campbell was raised in Lancaster, Pennsylvania. He attended Franklin &amp; Marshall College where he received a B.A. in Chemistry. He earned his M.D. degree from Temple University School of Medicine in Philadelphia and completed his internship at the Geisinger Clinic in Danville, PA. </p>
                    <p>He then served as a commissioned officer in the U.S. Public Health Service. Following this service, he completed his <a href="http://nclnet.org/health/eyes/providers.htm" target="_blank">ophthalmology</a> residency at the Mayo Clinic in Rochestor, Minnesota, after which he completed a two year fellowship in cornea and external disease at Harvard Medical School of Boston, Massachusetts. Dr. Campbell is certified by the American Board of Ophthalmology.</p>
                    <p>Prior to joining the staff of Parkwood Eye Center, Dr. Campbell practiced for 20 years in the Department of Ophthalmology of Park Nicollet Medical Center in Minneapolis, Minnesota.</p>                  <p>Dr. Campbell has been the Editor of Refractive Eyecare for Ophthalmologists, a member of the Editorial Board of Comprehensive Ophthalmology Update, and a Clinical Associate Professor of Ophthalmology, Wake Forest University School of Medicine.</p>
                    <p>He has spoken nationally and internationally on the subjects of contact lens technology, refractive eyecare and ocular infections. Dr. Campbell and his wife, Jo Ann, live in Elkin and have two grown sons, Scott and Ian. </p>
                    <p>Call today to schedule an appointment with Dr. Campbell - <strong>(336) 835-3400</strong></p></td>
              </tr>
          </table></td></tr>
    </table>      </td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="images/parkwood_image.gif" width="450" height="10"></td>
    <td width="200" align="center" valign="top" bgcolor="#003300"><img src="images/parkwood_image.gif" width="1" height="1"></td>
  </tr>
</table>
<?php include("includes/footer.htm"); ?>
<div align="center"></div>
<p align="center">&nbsp;</p>
<p align="center" class="size10font">
  <?php include("includes/mwd.htm"); ?>
</p>
<p align="center" class="size10font">&nbsp;</p>

</body>
</html>
