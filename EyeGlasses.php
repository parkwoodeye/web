<html>
<head>
<?php $title = 'Optical Services - Eye Glasses - New and Replacement';?>
<title><?php echo "$title"; ?> - Parkwood Eye Center - Elkin, NC</title>
<meta name="description" content="Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>
<body>
<table width="770" height="198" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td width="243" height="145"> <a href="/"><img src="images/parkwoodeyecenterlogo.jpg" alt="Back to Parkwood Eye Center Home Page." width="243" height="145" border="0"></a></td>
    <td rowspan="2"> <a href="/"><img src="images/random/glassescomputer.jpg" alt="Parkwood Eye Center - Serving the Yadkin Valley - Back to Home Page." width="527" height="198" hspace="0" border="0"></a></td>
  </tr>
  <tr>
    <td><div align="center"><span class="size18times"><span class="size24">(336) 835-3400</span></span></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="middle" bgcolor="#000066"><div align="center"><?php include("includes/topnav.htm"); ?></div></td>
    <td width="200" rowspan="2" align="center" valign="top" bgcolor="#003300"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td align="center" valign="top"><?php include("includes/rightnav.htm"); ?></td>
      </tr>
    </table>    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td align="left" valign="top"><h1 class="size18bold greentext"><?php echo "$title"; ?> </h1>
          <p>Parkwood Eye Center of Elkin, NC, offers comprehensive optical (eye glass) care which includes:</p>
          <ul>
            <li>
              <h4>The Latest Designer Eye Glass Frames </h4>
            </li>
            <li>
              <h4>Prescription Lenses</h4>
            </li>
            <li>
              <h4>Prescription Sunglasses</h4>
            </li>
            <li>
              <h4>Custom Fitting of Eye Glasses</h4>
            </li>
            <li>
              <h4>Replacement Lenses &amp; More</h4>
            </li>
          </ul>
          <p>We gladly accept eyeglass prescriptions from other optometrists and ophthalmologists' offices - but of course you can simply get your prescription from <a href="ElkinEyeDoctors.php">one of our Parkwood Eye Center doctors</a>.</p>
          <p>Our optical shop is conveniently located in the lobby of our ophthalmology office at 177 Parkwood Drive, Elkin, NC. </p>
          <table width="90%"  border="0" align="center" cellpadding="5" cellspacing="0">
            <tr bgcolor="#9B214A">
              <td colspan="3"><div align="center" class="size16boldwhite">We're Ready to Assist You</div></td>
              </tr>
            <tr>
              <td width="50%"><div align="center" class="style1">
                <p><img src="images/shirley.jpg" alt="Shirley Chappell." width="125" height="161" vspace="3"><br>
                  Shirley Chappell</p>
                </div></td>
              <td width="50%" align="center"><strong><img src="images/elaine.jpg" alt="Elaine Reavis." width="125" height="161" vspace="3"><br>
Elaine Reavis </strong></td>
              <td width="50%"><div align="center"><strong><img src="images/debbieharris.jpg" alt="Debbie Harris." width="125" height="161" vspace="3"><br>
                Debbie Harris             </strong></div></td>
            </tr>
          </table>          
          <p>If you have a question, would like to discuss your eye glass needs or are a current patient at Parkwood Eye Center, please feel free to give us a call (<strong>336-835-3400</strong>) or <a href="DrivingDirections.php">come by our optical shop</a> to discuss your lens needs with one of our  opticians.</p></td>
      </tr>
    </table>      </td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="images/parkwood_image.gif" width="450" height="10"></td>
    <td width="200" align="center" valign="top" bgcolor="#003300"><img src="images/parkwood_image.gif" width="1" height="1"></td>
  </tr>
</table>
<?php include("includes/footer.htm"); ?>
<div align="center"></div>
<p align="center">&nbsp;</p>
<p align="center" class="size10font">
  <?php include("includes/mwd.htm"); ?>
</p>
<p align="center" class="size10font">&nbsp;</p>

</body>
</html>
