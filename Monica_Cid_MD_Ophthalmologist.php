<html>
<head>
<?php $title = 'Monica Cid - Ophthalmologist';?>
<title><?php echo "$title"; ?> - Parkwood Eye Center - Elkin, NC</title>
<meta name="description" content="Monica Cid - Serving the Yadkin Valley's eye care needs since 1991. The experienced and caring professionals at Parkwood Eye Center provide quality comprehensive ophthalmology services to the residents of North-Western North Carolina.">
<meta name="keywords" content="Monica Cid, Parkwood, Eye, Doctors, ophthalmology, Eyecare, Care, Elkin, NC, North Carolina, Jonesville">
<META NAME="Author" CONTENT="Parkwood Eye Center - Elkin, NC">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="includes/parkwood.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="770" height="198" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
  <tr>
    <td width="243" height="145"> <a href="/"><img src="images/parkwoodeyecenterlogo.jpg" alt="Back to Parkwood Eye Center Home Page." width="243" height="145" border="0"></a></td>
    <td rowspan="2"> <img src="images/random/blueridgemtns.jpg" alt="Parkwood Eye Center - Serving the Yadkin Valley - Back to Home Page." width="527" height="198" hspace="0" border="0"></td>
  </tr>
  <tr>
    <td><div align="center"><span class="size18times"><span class="size24">(336) 835-3400</span></span></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><div align="center"><img src="images/parkwood_image.gif" width="529" height="7"></div></td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="middle" bgcolor="#000066"><div align="center"><?php include("includes/topnav.htm"); ?></div></td>
    <td width="200" rowspan="2" align="center" valign="top" bgcolor="#003300"><table width="100%"  border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td align="center" valign="top"><p class="topnavlinks"><span class="rightnav"><br>
          Our doctors look forward to serving your eye care needs.</span></p>
          <p class="topnavlinks"><hr></p>
          <p class="topnavlinks">
            <?php include("includes/rightnav.htm"); ?>
          </p></td>
      </tr>
    </table>    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="8">
      <tr>
        <td align="left" valign="top">            <table width="100%"  border="0" align="center" cellpadding="10" cellspacing="0">
              <tr>
                <td width="50%"><div align="center">
                    <h2 align="center"><strong><img src="images/photo_Monica_Cid_MD_Ophthalmologist.jpg" alt="Monica Cid, MD - Medical Eye Doctor - Opthamologist." width="175" height="225" vspace="3" align="right">Monica Cid, MD</strong> </h2>
                  </div>
                    <p>Dr. Cid grew up in Wisconsin and received her Bachelor of Science degree from the University of Wisconsin-Milwaukee where her honors included graduating Magna Cum Laude and Phi Beta Kappa.&nbsp; She earned her Medical Doctor degree from the Medical College of Wisconsin and completed her internship at Gundersen Lutheran Hospital in Lacrosse, Wisconsin.&nbsp; </p>
                    <p>She completed her ophthalmology residency at the University of Missouri-Kansas City and then did a refractive surgery fellowship at TEC University in Monterrey, Mexico.&nbsp; Dr. Cid is certified by the American Board of Ophthalmology and has been in active practice since 1999.&nbsp; Prior to joining Parkwood Eye Center, she practiced in Pittsburgh, Pennsylvania and Madrid, Verin, and Vigo, Spain, and has performed over 8,000 cataract and intraocular lens implant surgeries as well as thousands of various laser surgeries involving the treatment of retinal, corneal, and macular conditions of the eye.</p>
                    <p>Dr Cid is an active member of the American Society of Cataract and Refractive Surgeons.&nbsp; She has given numerous presentations on various topics related to cataract surgery and intraocular lens implantation at ophthalmology meetings in the U.S. and abroad.</p>
<p>Call today to schedule an appointment with Dr. Cid - <strong>(336) 835-3400</strong></p></td>
              </tr>
          </table></td></tr>
    </table>      </td>
  </tr>
</table>
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="images/parkwood_image.gif" width="450" height="10"></td>
    <td width="200" align="center" valign="top" bgcolor="#003300"><img src="images/parkwood_image.gif" width="1" height="1"></td>
  </tr>
</table>
<?php include("includes/footer.htm"); ?>
<div align="center"></div>
<p align="center">&nbsp;</p>
<p align="center" class="size10font">
  <?php include("includes/mwd.htm"); ?>
</p>
<p align="center" class="size10font">&nbsp;</p>

</body>
</html>
